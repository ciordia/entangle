# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Valentin Laskov <laskov@festa.bg>, 2012-2013
# Valentin Laskov <laskov@festa.bg>, 2015. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-10 22:49+0000\n"
"PO-Revision-Date: 2015-06-21 03:14-0400\n"
"Last-Translator: Valentin Laskov <laskov@festa.bg>\n"
"Language-Team: Bulgarian (http://www.transifex.com/projects/p/entangle/"
"language/bg/)\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.8.2\n"

#: ../src/backend/entangle-camera-list.c:266
#: ../src/backend/entangle-camera.c:765
#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr "Не мога да инициализирам способностите на gphoto2"

#: ../src/backend/entangle-camera-list.c:269
#: ../src/backend/entangle-camera.c:771
#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr "Не мога да заредя способностите на gphoto2"

#: ../src/backend/entangle-camera-list.c:415
#: ../src/backend/entangle-camera.c:777
#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr "Не мога да инициализирам портовете на gphoto2"

#: ../src/backend/entangle-camera-list.c:418
#: ../src/backend/entangle-camera.c:783
#, c-format
msgid "Cannot load gphoto2 ports"
msgstr "Не мога да заредя портовете на gphoto2"

#: ../src/backend/entangle-camera.c:814
#, c-format
msgid "Unable to initialize camera: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1170
#, c-format
msgid "Cannot capture image while not connected"
msgstr "Не мога да правя снимка докато не съм свързан"

#: ../src/backend/entangle-camera.c:1183
#, c-format
msgid "Unable to capture image: %s"
msgstr "Невъзможно е правенето на снимки: %s"

#: ../src/backend/entangle-camera.c:1297
#, c-format
msgid "Cannot preview image while not connected"
msgstr "Не мога да покажа предв. преглед докато не съм свързан"

#: ../src/backend/entangle-camera.c:1312
#, c-format
msgid "Unable to capture preview: %s"
msgstr "Невъзможен предварителен преглед: %s"

#: ../src/backend/entangle-camera.c:1319 ../src/backend/entangle-camera.c:1477
#, c-format
msgid "Unable to get file data: %s"
msgstr "Невъзможно вземането данните на файла: %s"

#: ../src/backend/entangle-camera.c:1325
#, c-format
msgid "Unable to get filename: %s"
msgstr "Невъзможно вземането името на файла: %s"

#: ../src/backend/entangle-camera.c:1447
#, c-format
msgid "Cannot download file while not connected"
msgstr "Не мога да сваля файл докато не съм свързан"

#: ../src/backend/entangle-camera.c:1470
#, c-format
msgid "Unable to get camera file: %s"
msgstr "Невъзможно да взема файла на камерата: %s"

#: ../src/backend/entangle-camera.c:1597
#, c-format
msgid "Cannot delete file while not connected"
msgstr "Не мога да изтрия файл докато не съм свързан"

#: ../src/backend/entangle-camera.c:1615
#, c-format
msgid "Unable to delete file: %s"
msgstr "Невъзможно изтриване на файла: %s"

#: ../src/backend/entangle-camera.c:1731
#, c-format
msgid "Cannot wait for events while not connected"
msgstr "Не мога да очаквам събития докато не съм свързан"

#: ../src/backend/entangle-camera.c:1757
#, c-format
msgid "Unable to wait for events: %s"
msgstr "Невъзможно очакването на събития: %s"

#: ../src/backend/entangle-camera.c:1952 ../src/backend/entangle-camera.c:2115
#: ../src/backend/entangle-camera.c:2251 ../src/backend/entangle-camera.c:2706
#: ../src/backend/entangle-camera.c:2881 ../src/backend/entangle-camera.c:3042
#: ../src/backend/entangle-camera.c:3266 ../src/backend/entangle-camera.c:3432
#, c-format
msgid "Unable to fetch widget type"
msgstr "Невъзможно да се извлече типа на джаджата"

#: ../src/backend/entangle-camera.c:1958 ../src/backend/entangle-camera.c:2121
#: ../src/backend/entangle-camera.c:2257
#, c-format
msgid "Unable to fetch widget name"
msgstr "Невъзможно да се извлече името на джаджата"

#: ../src/backend/entangle-camera.c:2370
#, c-format
msgid "Unable to load controls, camera is not connected"
msgstr "Не мога да заредя управлението, камерата не е свързана"

#: ../src/backend/entangle-camera.c:2379
#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:2500
#, c-format
msgid "Unable to save controls, camera is not connected"
msgstr "Невъзможно да запиша управлението, камерата не е свързана"

#: ../src/backend/entangle-camera.c:2506
#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr "Невъзможно да запиша управлението, камерата не може да се конфигурира"

#: ../src/backend/entangle-camera.c:2525 ../src/backend/entangle-camera.c:2742
#: ../src/backend/entangle-camera.c:2911 ../src/backend/entangle-camera.c:3102
#: ../src/backend/entangle-camera.c:3288 ../src/backend/entangle-camera.c:3456
#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:2628 ../src/backend/entangle-camera.c:2682
#: ../src/backend/entangle-camera.c:2860 ../src/backend/entangle-camera.c:3021
#: ../src/backend/entangle-camera.c:3245 ../src/backend/entangle-camera.c:3411
#, c-format
msgid "Controls not available when camera is disconnected"
msgstr "Управлението не е налично, докато камерата е изключена"

#: ../src/backend/entangle-camera.c:2634 ../src/backend/entangle-camera.c:2688
#: ../src/backend/entangle-camera.c:2866 ../src/backend/entangle-camera.c:3027
#: ../src/backend/entangle-camera.c:3251 ../src/backend/entangle-camera.c:3417
#, c-format
msgid "Controls not available for this camera"
msgstr "Управление не е налично за тази камера"

#: ../src/backend/entangle-camera.c:2699
#, c-format
msgid "Viewfinder control not available with this camera"
msgstr "Управлението на визьора не е достъпно за тази камера"

#: ../src/backend/entangle-camera.c:2712
#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr ""

#: ../src/backend/entangle-camera.c:2726 ../src/backend/entangle-camera.c:2733
#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr "Провал при задаване състоянието на визьора: %s %d"

#: ../src/backend/entangle-camera.c:2874
#, c-format
msgid "Autofocus control not available with this camera"
msgstr "Управлението на автофокуса не е достъпно за тази камера"

#: ../src/backend/entangle-camera.c:2887
#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr ""

#: ../src/backend/entangle-camera.c:2894 ../src/backend/entangle-camera.c:2902
#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr "Провал при задаване състоянието на автофокуса: %s %d"

#: ../src/backend/entangle-camera.c:3035
#, c-format
msgid "Manual focus control not available with this camera"
msgstr "Управлението на ръчния фокус не е достъпно за тази камера"

#: ../src/backend/entangle-camera.c:3049
#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3068 ../src/backend/entangle-camera.c:3092
#: ../src/backend/entangle-camera.c:3111 ../src/backend/entangle-camera.c:3127
#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr "Провал при задаване състоянието на ръчния фокус: %s %d"

#: ../src/backend/entangle-camera.c:3085 ../src/backend/entangle-camera.c:3120
#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr "Провал при четене ръчния избор на фокус %d: %s %d"

#: ../src/backend/entangle-camera.c:3259
#, c-format
msgid "Time setting not available with this camera"
msgstr "Управлението на часовника не е достъпно за тази камера"

#: ../src/backend/entangle-camera.c:3272
#, c-format
msgid "Time setting was not a date widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3279
#, c-format
msgid "Failed to set time state: %s %d"
msgstr "Провал при задаване състоянието на часовника: %s %d"

#: ../src/backend/entangle-camera.c:3425
#, c-format
msgid "Capture target setting not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:3438
#, c-format
msgid "Time setting was not a choice widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3447
#, c-format
msgid "Failed to set capture target: %s %d"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:600
#: ../src/frontend/entangle-camera-manager.ui.h:24
msgid "Capture an image"
msgstr "Създава снимка"

#: ../src/frontend/entangle-camera-manager.c:601
#: ../src/frontend/entangle-camera-manager.ui.h:25
msgid "Continuous capture preview"
msgstr "Постоянен изглед"

#: ../src/frontend/entangle-camera-manager.c:605
msgid "This camera does not support image capture"
msgstr "Камерата не поддържа снимане на изображение"

#: ../src/frontend/entangle-camera-manager.c:608
msgid "This camera does not support image preview"
msgstr "Тази камера не поддържа предв. преглед на изображения"

#: ../src/frontend/entangle-camera-manager.c:750
#, c-format
msgid "Operation: %s"
msgstr "Действие: %s"

#: ../src/frontend/entangle-camera-manager.c:752
msgid "Entangle: Operation failed"
msgstr "Entangle: Действието не успя"

#: ../src/frontend/entangle-camera-manager.c:784
#: ../src/frontend/entangle-camera-manager.c:1063
msgid "Camera load controls failed"
msgstr "Зареждането на управлението на камерата не успя"

#: ../src/frontend/entangle-camera-manager.c:786
#: ../src/frontend/entangle-camera-manager.c:1065
msgid "Entangle: Camera load controls failed"
msgstr "Entangle: Зареждането на управлението на камерата не успя"

#: ../src/frontend/entangle-camera-manager.c:824
msgid "Monitor"
msgstr "Монитор"

#: ../src/frontend/entangle-camera-manager.c:1025
msgid "Camera Manager - Entangle"
msgstr "Управление на камерата - Entangle"

#: ../src/frontend/entangle-camera-manager.c:1114
#: ../src/frontend/entangle-camera-manager.c:2524
#, c-format
msgid "Unable to connect to camera: %s"
msgstr "Не мога да се свържа към камерата: %s"

#: ../src/frontend/entangle-camera-manager.c:1119
#: ../src/frontend/entangle-camera-manager.c:2530
msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""
"Проверете дали камерата не е\n"
"\n"
" - отворена от друго фото <b>приложение</b>\n"
" - монтирана като <b>файлова система</b> на десктопа\n"
" - в <b>спящ режим</b> за пестене на енергия\n"

#: ../src/frontend/entangle-camera-manager.c:1124
#: ../src/frontend/entangle-camera-manager.c:2535
#: ../src/frontend/entangle-camera-manager.ui.h:9
msgid "Cancel"
msgstr "Отказ"

#: ../src/frontend/entangle-camera-manager.c:1125
#: ../src/frontend/entangle-camera-manager.c:2536
msgid "Retry"
msgstr "Опит отново"

#: ../src/frontend/entangle-camera-manager.c:1156
msgid "Camera is in use"
msgstr "Камерата се ползва"

#: ../src/frontend/entangle-camera-manager.c:1160
msgid ""
"The camera cannot be opened because it is currently mounted as a filesystem. "
"Do you wish to umount it now ?"
msgstr ""
"Не мога да отворя камерата, понеже в момента е монтирана като файлова "
"система. Искате ли да я демонтирам сега?"

#: ../src/frontend/entangle-camera-manager.c:1164
#: ../src/frontend/entangle-camera-picker.c:130
msgid "No"
msgstr "Не"

#: ../src/frontend/entangle-camera-manager.c:1165
#: ../src/frontend/entangle-camera-picker.c:130
msgid "Yes"
msgstr "Да"

#: ../src/frontend/entangle-camera-manager.c:1200
msgid "Camera connect failed"
msgstr "Свързването към камера не успя"

#: ../src/frontend/entangle-camera-manager.c:1202
msgid "Entangle: Camera connect failed"
msgstr "Entangle: Свързването към камера не успя"

#: ../src/frontend/entangle-camera-manager.c:1237
#, c-format
msgid "%s Camera Manager - Entangle"
msgstr "Управление на камера %s - Entangle"

#: ../src/frontend/entangle-camera-manager.c:1533
#: ../src/frontend/entangle-camera-manager.ui.h:22
msgid "Select a folder"
msgstr "Изберете папка"

#: ../src/frontend/entangle-camera-manager.c:1536
msgid "_Cancel"
msgstr "От_каз"

#: ../src/frontend/entangle-camera-manager.c:1538
msgid "_Open"
msgstr "_Отваряне"

#: ../src/frontend/entangle-camera-manager.c:1717
#: ../src/frontend/entangle-camera-manager.ui.h:7
#: ../src/frontend/entangle-camera-picker.c:512
#: ../src/frontend/entangle-preferences-display.c:989
#: ../src/frontend/entangle-preferences-display.ui.h:12
msgid "Capture"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1738
msgid "Script"
msgstr "Скрипт"

#: ../src/frontend/entangle-camera-manager.c:1806
#: ../src/frontend/entangle-camera-manager.ui.h:8
msgid "Preview"
msgstr "Преглед"

#: ../src/frontend/entangle-camera-manager.c:1961
msgid "Set clock"
msgstr "Сверяване на часовника"

#: ../src/frontend/entangle-camera-manager.c:2004
msgid "Autofocus failed"
msgstr "Автофокусът се провали"

#: ../src/frontend/entangle-camera-manager.c:2006
msgid "Entangle: Camera autofocus failed"
msgstr "Entangle: Автофокусът на камерата се провали"

#: ../src/frontend/entangle-camera-manager.c:2036
msgid "Manual focus failed"
msgstr "Ръчният фокус се провали"

#: ../src/frontend/entangle-camera-manager.c:2038
msgid "Entangle: Camera manual focus failed"
msgstr "Entangle: Ръчният фокус на камерата се провали"

#: ../src/frontend/entangle-camera-manager.c:2653
msgid "Delete"
msgstr "Изтрий"

#: ../src/frontend/entangle-camera-manager.c:2782
msgid "Select application..."
msgstr "Изберете приложение..."

#: ../src/frontend/entangle-camera-manager.c:3010
msgid "Image histogram"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:3012
msgid "Automation"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-manager.ui.h:1
#: ../src/entangle.desktop.in.h:1
msgid "Entangle"
msgstr "Entangle"

#: ../src/frontend/entangle-camera-manager.ui.h:2
msgid "_File"
msgstr "_Файл"

#: ../src/frontend/entangle-camera-manager.ui.h:3
msgid "Select session"
msgstr "Избор на сесия"

#: ../src/frontend/entangle-camera-manager.ui.h:4
msgid "_Edit"
msgstr "_Редактиране"

#: ../src/frontend/entangle-camera-manager.ui.h:5
msgid "Camera"
msgstr "Камера"

#: ../src/frontend/entangle-camera-manager.ui.h:6
msgid "Connect"
msgstr "Свързване"

#: ../src/frontend/entangle-camera-manager.ui.h:10
msgid "Synchronize clock"
msgstr "Синхронизирай часовника"

#: ../src/frontend/entangle-camera-manager.ui.h:11
msgid "_View"
msgstr "_Изглед"

#: ../src/frontend/entangle-camera-manager.ui.h:12
msgid "Settings"
msgstr "Настройки"

#: ../src/frontend/entangle-camera-manager.ui.h:13
msgid "Fullscreen"
msgstr "Цял екран"

#: ../src/frontend/entangle-camera-manager.ui.h:14
msgid "Presentation"
msgstr "Представяне"

#: ../src/frontend/entangle-camera-manager.ui.h:15
msgid "Present on"
msgstr "Съществува в"

#: ../src/frontend/entangle-camera-manager.ui.h:16
msgid "Windows"
msgstr "Прозорци"

#: ../src/frontend/entangle-camera-manager.ui.h:17
msgid "New window"
msgstr "Нов прозорец"

#: ../src/frontend/entangle-camera-manager.ui.h:18
msgid "Synchronize capture"
msgstr "Синхронизирай снимането"

#: ../src/frontend/entangle-camera-manager.ui.h:19
msgid "_Help"
msgstr "_Помощ"

#: ../src/frontend/entangle-camera-manager.ui.h:20
msgid "Manual"
msgstr "Ръководство"

#: ../src/frontend/entangle-camera-manager.ui.h:21
msgid "Supported Cameras"
msgstr "Поддържани камери"

#: ../src/frontend/entangle-camera-manager.ui.h:23
msgid "View the full list of camera settings"
msgstr "Преглед на всички настройки на камерата"

#: ../src/frontend/entangle-camera-manager.ui.h:26
msgid ""
"Attempt to cancel the currently\n"
"executing camera operation"
msgstr ""
"Опит за спиране на текущо\n"
"изпълняваната от камерата операция"

#: ../src/frontend/entangle-camera-manager.ui.h:28
msgid ""
"Shrink the display of the image\n"
"by one factor"
msgstr ""
"Смалява показването на образа\n"
"с една стъпка"

#: ../src/frontend/entangle-camera-manager.ui.h:30
msgid "Zoom Out"
msgstr "Уголемяване"

#: ../src/frontend/entangle-camera-manager.ui.h:31
msgid ""
"Enlarge the display of the image\n"
"by one factor"
msgstr ""
"Уголемява показването на образа\n"
"с една стъпка"

#: ../src/frontend/entangle-camera-manager.ui.h:33
msgid "Zoom In"
msgstr "Смаляване"

#: ../src/frontend/entangle-camera-manager.ui.h:34
msgid ""
"Resize the image to automatically\n"
"fit the available window real estate"
msgstr ""
"Автоматично преоразмеряване на образа\n"
"според достъпното пространство в прозореца"

#: ../src/frontend/entangle-camera-manager.ui.h:36
msgid "Best Fit"
msgstr "Най-добро вместване"

#: ../src/frontend/entangle-camera-manager.ui.h:37
msgid ""
"Display the image with its\n"
"native pixel size"
msgstr ""
"Показва образа в неговия\n"
"оригинален размер в пиксели"

#: ../src/frontend/entangle-camera-manager.ui.h:39
msgid "Normal"
msgstr "Нормален"

#: ../src/frontend/entangle-camera-manager.ui.h:40
msgid "Switch to fullscreen display mode"
msgstr "Превключва изображението на цял екран"

#: ../src/frontend/entangle-camera-manager.ui.h:41
msgid "Full Screen"
msgstr "Цял екран"

#: ../src/frontend/entangle-camera-manager.ui.h:42
msgid "Open with"
msgstr "Отваряне с"

#: ../src/frontend/entangle-camera-picker.c:510
msgid "Model"
msgstr "Модел"

#: ../src/frontend/entangle-camera-picker.c:511
msgid "Port"
msgstr "Порт"

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-picker.ui.h:1
msgid "Select camera - Entangle"
msgstr "Избор на камера - Entangle"

#: ../src/frontend/entangle-camera-picker.ui.h:2
msgid "Select a camera to connect to:"
msgstr "Избор на камера за използване:"

#: ../src/frontend/entangle-camera-picker.ui.h:3
msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh"
msgstr ""
"Няма открити камери, проверете дали\n"
"\n"
"  - <b>кабелите</b> са свързани\n"
"  - камерата е <b>включена</b>\n"
"  - камерата е в <b>необходимия режим</b>\n"
"  - моделът на камерата се <b>поддържа</b>\n"
"\n"
"USB камерите се откриват автоматично в момента\n"
"на включване. За останалите, пробвайте обновяване."

#: ../src/frontend/entangle-camera-support.c:227
msgid "capture"
msgstr "снимане"

#: ../src/frontend/entangle-camera-support.c:233
msgid "preview"
msgstr ""

#: ../src/frontend/entangle-camera-support.c:239
msgid "settings"
msgstr "настройки"

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-support.ui.h:1
msgid "label"
msgstr "етикет"

#: ../src/frontend/entangle-control-panel.c:86
msgid "Camera control update failed"
msgstr "Обновяването на управлението на камерата не успя"

#: ../src/frontend/entangle-control-panel.c:88
msgid "Entangle: Camera control update failed"
msgstr "Entangle: Обновяването на управлението на камерата не успя"

#: ../src/frontend/entangle-control-panel.c:316
msgid "On"
msgstr "Вкл."

#: ../src/frontend/entangle-control-panel.c:316
msgid "Off"
msgstr "Изкл."

#: ../src/frontend/entangle-control-panel.c:771
msgid "Reset controls"
msgstr "Връщане към нормално състояние"

#: ../src/frontend/entangle-control-panel.c:843
msgid "No camera connected"
msgstr "Няма свързана камера"

#: ../src/frontend/entangle-control-panel.c:850
msgid "No controls available"
msgstr "Нищо достъпно за контролиране"

#: ../src/frontend/entangle-dpms.c:59
msgid "Screen blanking is not available on this display"
msgstr "Screen blanking не е налично за този дисплей"

#: ../src/frontend/entangle-dpms.c:71
msgid "Screen blanking is not implemented on this platform"
msgstr "Screen blanking не е реализирано за тази платформа"

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-help-about.ui.h:1
msgid "About - Entangle"
msgstr "Относно - Entangle"

#: ../src/frontend/entangle-help-about.ui.h:2 ../src/entangle.desktop.in.h:2
msgid "Tethered Camera Control & Capture"
msgstr "Свързани контрол на камерата и снимане"

#: ../src/frontend/entangle-preferences-display.c:975
#: ../src/frontend/entangle-preferences-display.ui.h:27
msgid "Interface"
msgstr "Интерфейс"

#: ../src/frontend/entangle-preferences-display.c:982
#: ../src/frontend/entangle-preferences-display.ui.h:39
msgid "Image Viewer"
msgstr "Преглед на изображението"

#: ../src/frontend/entangle-preferences-display.c:996
#: ../src/frontend/entangle-preferences-display.ui.h:20
msgid "Color Management"
msgstr "Управление на цветовете"

#: ../src/frontend/entangle-preferences-display.c:1003
#: ../src/frontend/entangle-preferences-display.ui.h:22
msgid "Plugins"
msgstr "Плъгини"

#: ../src/frontend/entangle-preferences-display.c:1024
msgid "ICC profiles (*.icc, *.icm)"
msgstr "ICC профили (*.icc, *.icm)"

#: ../src/frontend/entangle-preferences-display.c:1029
msgid "All files (*.*)"
msgstr "Всички файлове (*.*)"

#: ../src/frontend/entangle-preferences-display.c:1055
msgid "1:1 - Square / MF 6x6"
msgstr "1:1 - Квадрат / MF 6x6"

#: ../src/frontend/entangle-preferences-display.c:1060
msgid "1.15:1 - Movietone"
msgstr "1.15:1 - Movietone"

#: ../src/frontend/entangle-preferences-display.c:1065
msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"

#: ../src/frontend/entangle-preferences-display.c:1070
msgid "1.37:1 - 35mm movie"
msgstr "1.37:1 - 35mm филм"

#: ../src/frontend/entangle-preferences-display.c:1075
msgid "1.44:1 - IMAX"
msgstr "1.44:1 - IMAX"

#: ../src/frontend/entangle-preferences-display.c:1080
msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr "1.50:1 (3:2, 15:10)- 35mm SLR"

#: ../src/frontend/entangle-preferences-display.c:1085
msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr "1.6:1 (8:5, 16:10) - Широк екран"

#: ../src/frontend/entangle-preferences-display.c:1090
msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr "1.66:1 (5:3, 15:9) - Super 16mm"

#: ../src/frontend/entangle-preferences-display.c:1095
msgid "1.75:1 (7:4) - Widescreen"
msgstr "1.75:1 (7:4) - Широк екран"

#: ../src/frontend/entangle-preferences-display.c:1100
msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr "1.77:1 (16:9) - APS-H / HDTV / Широк екран"

#: ../src/frontend/entangle-preferences-display.c:1105
msgid "1.85:1 - 35mm Widescreen"
msgstr "1.85:1 - 35mm Широк екран"

#: ../src/frontend/entangle-preferences-display.c:1110
msgid "2.00:1 - SuperScope"
msgstr "2.00:1 - SuperScope"

#: ../src/frontend/entangle-preferences-display.c:1115
msgid "2.10:1 (21:10) - Planned HDTV"
msgstr "2.10:1 (21:10) - Planned HDTV"

#: ../src/frontend/entangle-preferences-display.c:1120
msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr "2.20:1 (11:5, 22:10) - 70mm филм"

#: ../src/frontend/entangle-preferences-display.c:1125
msgid "2.35:1 - CinemaScope"
msgstr "2.35:1 - CinemaScope"

#: ../src/frontend/entangle-preferences-display.c:1130
msgid "2.37:1 (64:27)- HDTV cinema"
msgstr "2.37:1 (64:27)- HDTV cinema"

#: ../src/frontend/entangle-preferences-display.c:1135
msgid "2.39:1 (12:5)- Panavision"
msgstr "2.39:1 (12:5)- Panavision"

#: ../src/frontend/entangle-preferences-display.c:1140
msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr "2.55:1 (23:9)- CinemaScope 55"

#: ../src/frontend/entangle-preferences-display.c:1145
msgid "2.59:1 (13:5)- Cinerama"
msgstr "2.59:1 (13:5)- Cinerama"

#: ../src/frontend/entangle-preferences-display.c:1150
msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr "2.66:1 (8:3, 24:9)- Super 16mm"

#: ../src/frontend/entangle-preferences-display.c:1155
msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr "2.76:1 (11:4) - Ultra Panavision"

#: ../src/frontend/entangle-preferences-display.c:1160
msgid "2.93:1 - MGM Camera 65"
msgstr "2.93:1 - MGM Camera 65"

#: ../src/frontend/entangle-preferences-display.c:1165
msgid "3:1 APS Panorama"
msgstr "3:1 APS Panorama"

#: ../src/frontend/entangle-preferences-display.c:1170
msgid "4.00:1 - Polyvision"
msgstr "4.00:1 - Polyvision"

#: ../src/frontend/entangle-preferences-display.c:1175
msgid "12.00:1 - Circle-Vision 360"
msgstr "12.00:1 - Circle-Vision 360"

#: ../src/frontend/entangle-preferences-display.c:1193
msgid "None"
msgstr "Нищо"

#: ../src/frontend/entangle-preferences-display.c:1198
msgid "Center lines"
msgstr "Централни линии"

#: ../src/frontend/entangle-preferences-display.c:1203
msgid "Rule of 3rds"
msgstr "Правило на третините"

#: ../src/frontend/entangle-preferences-display.c:1208
msgid "Quarters"
msgstr "Четвъртини"

#: ../src/frontend/entangle-preferences-display.c:1213
msgid "Rule of 5ths"
msgstr "Правило на 5-ните"

#: ../src/frontend/entangle-preferences-display.c:1218
msgid "Golden sections"
msgstr "Златно сечение"

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-preferences-display.ui.h:1
msgid "Perceptual"
msgstr "Възприятие"

#: ../src/frontend/entangle-preferences-display.ui.h:2
msgid "Relative colourimetric"
msgstr "Относителна колориметрия"

#: ../src/frontend/entangle-preferences-display.ui.h:3
msgid "Saturation"
msgstr "Насищане"

#: ../src/frontend/entangle-preferences-display.ui.h:4
msgid "Absolute colourimetric"
msgstr "Абсолютна колориметрия"

#: ../src/frontend/entangle-preferences-display.ui.h:5
msgid "Entangle Preferences"
msgstr "Предпочитания за Entangle"

#: ../src/frontend/entangle-preferences-display.ui.h:6
msgid "<b>Capture</b>"
msgstr "<b>Снимане</b>"

#: ../src/frontend/entangle-preferences-display.ui.h:7
msgid "Filename pattern:"
msgstr "Шаблон за име на файл:"

#: ../src/frontend/entangle-preferences-display.ui.h:8
msgid "Continue preview mode after capture"
msgstr "Продължаващ след снимането режим на преглед"

#: ../src/frontend/entangle-preferences-display.ui.h:9
msgid "Use preview output as capture image"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:10
msgid "Delete file from camera after downloading"
msgstr "След свалянето, изтривай файла в камерата"

#: ../src/frontend/entangle-preferences-display.ui.h:11
msgid "Automatically synchronize camera clock"
msgstr "Автоматично синхронизирай часовника на камерата"

#: ../src/frontend/entangle-preferences-display.ui.h:13
msgid "<b>Colour management</b>"
msgstr "<b>Управление на цветовете</b>"

#: ../src/frontend/entangle-preferences-display.ui.h:14
msgid "Mode of operation:"
msgstr "Начин на работа:"

#: ../src/frontend/entangle-preferences-display.ui.h:15
msgid "RGB profile:"
msgstr "RGB профил:"

#: ../src/frontend/entangle-preferences-display.ui.h:16
msgid "Monitor profile:"
msgstr "Профил на монитора:"

#: ../src/frontend/entangle-preferences-display.ui.h:17
msgid "Rendering intent:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:18
msgid "Detect the system monitor profile"
msgstr "Откриване профила на системния монитор"

#: ../src/frontend/entangle-preferences-display.ui.h:19
msgid "Colour managed display"
msgstr "Дисплей с управляван цвят"

#: ../src/frontend/entangle-preferences-display.ui.h:21
msgid "<b>Plugins</b>"
msgstr "<b>Плъгини</b>"

#: ../src/frontend/entangle-preferences-display.ui.h:23
msgid "<b>Interface</b>"
msgstr "<b>Интерфейс</b>"

#: ../src/frontend/entangle-preferences-display.ui.h:24
msgid "Automatically connect to cameras at startup"
msgstr "Автоматично свързване на камери при стартиране"

#: ../src/frontend/entangle-preferences-display.ui.h:25
msgid "Blank screen when capturing images"
msgstr "Празен екран при заснемане на изображение"

#: ../src/frontend/entangle-preferences-display.ui.h:26
msgid "Show linear histogram"
msgstr "Покажи линейна хистограма"

#: ../src/frontend/entangle-preferences-display.ui.h:28
msgid "<b>Image Viewer</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:29
msgid "Apply mask to alter aspect ratio"
msgstr "Прилагай маска за промяна на съотношението"

#: ../src/frontend/entangle-preferences-display.ui.h:30
msgid "Aspect ratio:"
msgstr "Съотношение:"

#: ../src/frontend/entangle-preferences-display.ui.h:31
msgid "Mask opacity:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:32
msgid "Display focus point during preview"
msgstr "Показвай точката на фокус в предв. преглед"

#: ../src/frontend/entangle-preferences-display.ui.h:33
msgid "Grid lines:"
msgstr "Мрежа от линии:"

#: ../src/frontend/entangle-preferences-display.ui.h:34
msgid "Use embedded preview from raw files"
msgstr "Ползвай вградения предв. преглед от raw файлове"

#: ../src/frontend/entangle-preferences-display.ui.h:35
msgid "Overlay earlier images"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:36
msgid "Overlay layers:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:37
msgid "Background:"
msgstr "Фон:"

#: ../src/frontend/entangle-preferences-display.ui.h:38
msgid "Highlight:"
msgstr ""

#: ../src/frontend/entangle-script.c:97
msgid "Untitled script"
msgstr "Неозаглавен скрипт"

#: ../src/frontend/entangle-script-config.c:80
msgid "No script"
msgstr "Няма скрипт"

#: ../src/frontend/entangle-script-config.c:162
msgid "No config options"
msgstr "Няма конфигурационни опции"

#: ../src/frontend/entangle-script-simple.c:64
msgid "Missing 'execute' method implementation"
msgstr ""

#: ../src/frontend/entangle-window.c:60 ../src/frontend/entangle-window.c:78
#, c-format
msgid "Could not load user interface definition file: %s"
msgstr "Не мога да заредя файла с дефиниции на потребителския интерфейс: %s"

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/entangle.appdata.xml.in.h:1
msgid "Tethered Camera Control &amp; Capture"
msgstr ""

#: ../src/entangle.appdata.xml.in.h:2
msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""
"Entangle е програма за управление на цифрови камери, свързани към компютъра "
"чрез USB."

#: ../src/entangle.appdata.xml.in.h:3
msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""
"Entangle може да задейства затвора на камерата, за да създава нови снимки. "
"Когато се поддържа от камерата, преди снимането може да бъде показван "
"постоянно обновяващ се изглед на сцената. Изображенията ще бъдат сваляни и "
"показвани така, както са заснети от камерата. Entangle позволява също, "
"настройките на камерата да се променят от компютъра."

#: ../src/entangle.appdata.xml.in.h:4
msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""
"Entangle е съвместим с повечето DSLR фотоапарати на Nikon и Canon, някои от "
"техните компактни модели и различни камери от други производители."
