# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-10 22:49+0000\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: Slovenian\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0)\n"
"X-Generator: Zanata 3.5.1\n"

#: ../src/backend/entangle-camera-list.c:266
#: ../src/backend/entangle-camera.c:765
#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr ""

#: ../src/backend/entangle-camera-list.c:269
#: ../src/backend/entangle-camera.c:771
#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr ""

#: ../src/backend/entangle-camera-list.c:415
#: ../src/backend/entangle-camera.c:777
#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr ""

#: ../src/backend/entangle-camera-list.c:418
#: ../src/backend/entangle-camera.c:783
#, c-format
msgid "Cannot load gphoto2 ports"
msgstr ""

#: ../src/backend/entangle-camera.c:814
#, c-format
msgid "Unable to initialize camera: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1170
#, c-format
msgid "Cannot capture image while not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:1183
#, c-format
msgid "Unable to capture image: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1297
#, c-format
msgid "Cannot preview image while not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:1312
#, c-format
msgid "Unable to capture preview: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1319 ../src/backend/entangle-camera.c:1477
#, c-format
msgid "Unable to get file data: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1325
#, c-format
msgid "Unable to get filename: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1447
#, c-format
msgid "Cannot download file while not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:1470
#, c-format
msgid "Unable to get camera file: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1597
#, c-format
msgid "Cannot delete file while not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:1615
#, c-format
msgid "Unable to delete file: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1731
#, c-format
msgid "Cannot wait for events while not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:1757
#, c-format
msgid "Unable to wait for events: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:1952 ../src/backend/entangle-camera.c:2115
#: ../src/backend/entangle-camera.c:2251 ../src/backend/entangle-camera.c:2706
#: ../src/backend/entangle-camera.c:2881 ../src/backend/entangle-camera.c:3042
#: ../src/backend/entangle-camera.c:3266 ../src/backend/entangle-camera.c:3432
#, c-format
msgid "Unable to fetch widget type"
msgstr ""

#: ../src/backend/entangle-camera.c:1958 ../src/backend/entangle-camera.c:2121
#: ../src/backend/entangle-camera.c:2257
#, c-format
msgid "Unable to fetch widget name"
msgstr ""

#: ../src/backend/entangle-camera.c:2370
#, c-format
msgid "Unable to load controls, camera is not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:2379
#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:2500
#, c-format
msgid "Unable to save controls, camera is not connected"
msgstr ""

#: ../src/backend/entangle-camera.c:2506
#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr ""

#: ../src/backend/entangle-camera.c:2525 ../src/backend/entangle-camera.c:2742
#: ../src/backend/entangle-camera.c:2911 ../src/backend/entangle-camera.c:3102
#: ../src/backend/entangle-camera.c:3288 ../src/backend/entangle-camera.c:3456
#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr ""

#: ../src/backend/entangle-camera.c:2628 ../src/backend/entangle-camera.c:2682
#: ../src/backend/entangle-camera.c:2860 ../src/backend/entangle-camera.c:3021
#: ../src/backend/entangle-camera.c:3245 ../src/backend/entangle-camera.c:3411
#, c-format
msgid "Controls not available when camera is disconnected"
msgstr ""

#: ../src/backend/entangle-camera.c:2634 ../src/backend/entangle-camera.c:2688
#: ../src/backend/entangle-camera.c:2866 ../src/backend/entangle-camera.c:3027
#: ../src/backend/entangle-camera.c:3251 ../src/backend/entangle-camera.c:3417
#, c-format
msgid "Controls not available for this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:2699
#, c-format
msgid "Viewfinder control not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:2712
#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr ""

#: ../src/backend/entangle-camera.c:2726 ../src/backend/entangle-camera.c:2733
#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr ""

#: ../src/backend/entangle-camera.c:2874
#, c-format
msgid "Autofocus control not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:2887
#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr ""

#: ../src/backend/entangle-camera.c:2894 ../src/backend/entangle-camera.c:2902
#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr ""

#: ../src/backend/entangle-camera.c:3035
#, c-format
msgid "Manual focus control not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:3049
#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3068 ../src/backend/entangle-camera.c:3092
#: ../src/backend/entangle-camera.c:3111 ../src/backend/entangle-camera.c:3127
#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr ""

#: ../src/backend/entangle-camera.c:3085 ../src/backend/entangle-camera.c:3120
#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr ""

#: ../src/backend/entangle-camera.c:3259
#, c-format
msgid "Time setting not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:3272
#, c-format
msgid "Time setting was not a date widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3279
#, c-format
msgid "Failed to set time state: %s %d"
msgstr ""

#: ../src/backend/entangle-camera.c:3425
#, c-format
msgid "Capture target setting not available with this camera"
msgstr ""

#: ../src/backend/entangle-camera.c:3438
#, c-format
msgid "Time setting was not a choice widget"
msgstr ""

#: ../src/backend/entangle-camera.c:3447
#, c-format
msgid "Failed to set capture target: %s %d"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:600
#: ../src/frontend/entangle-camera-manager.ui.h:24
msgid "Capture an image"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:601
#: ../src/frontend/entangle-camera-manager.ui.h:25
msgid "Continuous capture preview"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:605
msgid "This camera does not support image capture"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:608
msgid "This camera does not support image preview"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:750
#, c-format
msgid "Operation: %s"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:752
msgid "Entangle: Operation failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:784
#: ../src/frontend/entangle-camera-manager.c:1063
msgid "Camera load controls failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:786
#: ../src/frontend/entangle-camera-manager.c:1065
msgid "Entangle: Camera load controls failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:824
msgid "Monitor"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1025
msgid "Camera Manager - Entangle"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1114
#: ../src/frontend/entangle-camera-manager.c:2524
#, c-format
msgid "Unable to connect to camera: %s"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1119
#: ../src/frontend/entangle-camera-manager.c:2530
msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1124
#: ../src/frontend/entangle-camera-manager.c:2535
#: ../src/frontend/entangle-camera-manager.ui.h:9
msgid "Cancel"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1125
#: ../src/frontend/entangle-camera-manager.c:2536
msgid "Retry"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1156
msgid "Camera is in use"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1160
msgid ""
"The camera cannot be opened because it is currently mounted as a filesystem. "
"Do you wish to umount it now ?"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1164
#: ../src/frontend/entangle-camera-picker.c:130
msgid "No"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1165
#: ../src/frontend/entangle-camera-picker.c:130
msgid "Yes"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1200
msgid "Camera connect failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1202
msgid "Entangle: Camera connect failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1237
#, c-format
msgid "%s Camera Manager - Entangle"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1533
#: ../src/frontend/entangle-camera-manager.ui.h:22
msgid "Select a folder"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1536
msgid "_Cancel"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1538
msgid "_Open"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1717
#: ../src/frontend/entangle-camera-manager.ui.h:7
#: ../src/frontend/entangle-camera-picker.c:512
#: ../src/frontend/entangle-preferences-display.c:989
#: ../src/frontend/entangle-preferences-display.ui.h:12
msgid "Capture"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1738
msgid "Script"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1806
#: ../src/frontend/entangle-camera-manager.ui.h:8
msgid "Preview"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:1961
msgid "Set clock"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2004
msgid "Autofocus failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2006
msgid "Entangle: Camera autofocus failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2036
msgid "Manual focus failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2038
msgid "Entangle: Camera manual focus failed"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2653
msgid "Delete"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:2782
msgid "Select application..."
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:3010
msgid "Image histogram"
msgstr ""

#: ../src/frontend/entangle-camera-manager.c:3012
msgid "Automation"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-manager.ui.h:1
#: ../src/entangle.desktop.in.h:1
msgid "Entangle"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:2
msgid "_File"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:3
msgid "Select session"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:4
msgid "_Edit"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:5
msgid "Camera"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:6
msgid "Connect"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:10
msgid "Synchronize clock"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:11
msgid "_View"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:12
msgid "Settings"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:13
msgid "Fullscreen"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:14
msgid "Presentation"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:15
msgid "Present on"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:16
msgid "Windows"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:17
msgid "New window"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:18
msgid "Synchronize capture"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:19
msgid "_Help"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:20
msgid "Manual"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:21
msgid "Supported Cameras"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:23
msgid "View the full list of camera settings"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:26
msgid ""
"Attempt to cancel the currently\n"
"executing camera operation"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:28
msgid ""
"Shrink the display of the image\n"
"by one factor"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:30
msgid "Zoom Out"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:31
msgid ""
"Enlarge the display of the image\n"
"by one factor"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:33
msgid "Zoom In"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:34
msgid ""
"Resize the image to automatically\n"
"fit the available window real estate"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:36
msgid "Best Fit"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:37
msgid ""
"Display the image with its\n"
"native pixel size"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:39
msgid "Normal"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:40
msgid "Switch to fullscreen display mode"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:41
msgid "Full Screen"
msgstr ""

#: ../src/frontend/entangle-camera-manager.ui.h:42
msgid "Open with"
msgstr ""

#: ../src/frontend/entangle-camera-picker.c:510
msgid "Model"
msgstr ""

#: ../src/frontend/entangle-camera-picker.c:511
msgid "Port"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-picker.ui.h:1
msgid "Select camera - Entangle"
msgstr ""

#: ../src/frontend/entangle-camera-picker.ui.h:2
msgid "Select a camera to connect to:"
msgstr ""

#: ../src/frontend/entangle-camera-picker.ui.h:3
msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh"
msgstr ""

#: ../src/frontend/entangle-camera-support.c:227
msgid "capture"
msgstr ""

#: ../src/frontend/entangle-camera-support.c:233
msgid "preview"
msgstr ""

#: ../src/frontend/entangle-camera-support.c:239
msgid "settings"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-camera-support.ui.h:1
msgid "label"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:86
msgid "Camera control update failed"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:88
msgid "Entangle: Camera control update failed"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:316
msgid "On"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:316
msgid "Off"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:771
msgid "Reset controls"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:843
msgid "No camera connected"
msgstr ""

#: ../src/frontend/entangle-control-panel.c:850
msgid "No controls available"
msgstr ""

#: ../src/frontend/entangle-dpms.c:59
msgid "Screen blanking is not available on this display"
msgstr ""

#: ../src/frontend/entangle-dpms.c:71
msgid "Screen blanking is not implemented on this platform"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-help-about.ui.h:1
msgid "About - Entangle"
msgstr ""

#: ../src/frontend/entangle-help-about.ui.h:2 ../src/entangle.desktop.in.h:2
msgid "Tethered Camera Control & Capture"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:975
#: ../src/frontend/entangle-preferences-display.ui.h:27
msgid "Interface"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:982
#: ../src/frontend/entangle-preferences-display.ui.h:39
msgid "Image Viewer"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:996
#: ../src/frontend/entangle-preferences-display.ui.h:20
msgid "Color Management"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1003
#: ../src/frontend/entangle-preferences-display.ui.h:22
msgid "Plugins"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1024
msgid "ICC profiles (*.icc, *.icm)"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1029
msgid "All files (*.*)"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1055
msgid "1:1 - Square / MF 6x6"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1060
msgid "1.15:1 - Movietone"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1065
msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1070
msgid "1.37:1 - 35mm movie"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1075
msgid "1.44:1 - IMAX"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1080
msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1085
msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1090
msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1095
msgid "1.75:1 (7:4) - Widescreen"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1100
msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1105
msgid "1.85:1 - 35mm Widescreen"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1110
msgid "2.00:1 - SuperScope"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1115
msgid "2.10:1 (21:10) - Planned HDTV"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1120
msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1125
msgid "2.35:1 - CinemaScope"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1130
msgid "2.37:1 (64:27)- HDTV cinema"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1135
msgid "2.39:1 (12:5)- Panavision"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1140
msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1145
msgid "2.59:1 (13:5)- Cinerama"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1150
msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1155
msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1160
msgid "2.93:1 - MGM Camera 65"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1165
msgid "3:1 APS Panorama"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1170
msgid "4.00:1 - Polyvision"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1175
msgid "12.00:1 - Circle-Vision 360"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1193
msgid "None"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1198
msgid "Center lines"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1203
msgid "Rule of 3rds"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1208
msgid "Quarters"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1213
msgid "Rule of 5ths"
msgstr ""

#: ../src/frontend/entangle-preferences-display.c:1218
msgid "Golden sections"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/frontend/entangle-preferences-display.ui.h:1
msgid "Perceptual"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:2
msgid "Relative colourimetric"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:3
msgid "Saturation"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:4
msgid "Absolute colourimetric"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:5
msgid "Entangle Preferences"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:6
msgid "<b>Capture</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:7
msgid "Filename pattern:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:8
msgid "Continue preview mode after capture"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:9
msgid "Use preview output as capture image"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:10
msgid "Delete file from camera after downloading"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:11
msgid "Automatically synchronize camera clock"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:13
msgid "<b>Colour management</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:14
msgid "Mode of operation:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:15
msgid "RGB profile:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:16
msgid "Monitor profile:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:17
msgid "Rendering intent:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:18
msgid "Detect the system monitor profile"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:19
msgid "Colour managed display"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:21
msgid "<b>Plugins</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:23
msgid "<b>Interface</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:24
msgid "Automatically connect to cameras at startup"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:25
msgid "Blank screen when capturing images"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:26
msgid "Show linear histogram"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:28
msgid "<b>Image Viewer</b>"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:29
msgid "Apply mask to alter aspect ratio"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:30
msgid "Aspect ratio:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:31
msgid "Mask opacity:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:32
msgid "Display focus point during preview"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:33
msgid "Grid lines:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:34
msgid "Use embedded preview from raw files"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:35
msgid "Overlay earlier images"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:36
msgid "Overlay layers:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:37
msgid "Background:"
msgstr ""

#: ../src/frontend/entangle-preferences-display.ui.h:38
msgid "Highlight:"
msgstr ""

#: ../src/frontend/entangle-script.c:97
msgid "Untitled script"
msgstr ""

#: ../src/frontend/entangle-script-config.c:80
msgid "No script"
msgstr ""

#: ../src/frontend/entangle-script-config.c:162
msgid "No config options"
msgstr ""

#: ../src/frontend/entangle-script-simple.c:64
msgid "Missing 'execute' method implementation"
msgstr ""

#: ../src/frontend/entangle-window.c:60 ../src/frontend/entangle-window.c:78
#, c-format
msgid "Could not load user interface definition file: %s"
msgstr ""

#.
#. * Local variables:
#. *  c-indent-level: 4
#. *  c-basic-offset: 4
#. *  indent-tabs-mode: nil
#. *  tab-width: 8
#. * End:
#.
#: ../src/entangle.appdata.xml.in.h:1
msgid "Tethered Camera Control &amp; Capture"
msgstr ""

#: ../src/entangle.appdata.xml.in.h:2
msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""

#: ../src/entangle.appdata.xml.in.h:3
msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""

#: ../src/entangle.appdata.xml.in.h:4
msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""
